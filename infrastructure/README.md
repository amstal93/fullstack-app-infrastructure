# Spinup
Manual way of setting up the pre `Initialization Infrastructure`

```
aws iam create-user --user-name klingon
aws iam create-access-key --user-name klingon
aws iam attach-user-policy --policy-arn arn:aws:iam::aws:policy/AdministratorAccess --user-name klingon

aws dynamodb create-table --table-name klingon-terraform-state --attribute-definitions AttributeName=LockID,AttributeType=S --key-schema AttributeName=LockID,KeyType=HASH --provisioned-throughput ReadCapacityUnits=5,WriteCapacityUnits=5

aws s3api create-bucket --bucket klingon-terraform-state --acl private --region us-east-1
aws s3api put-bucket-versioning --bucket klingon-terraform-state --versioning-configuration Status=Enabled
aws s3api put-public-access-block --bucket klingon-terraform-state --public-access-block-configuration "BlockPublicAcls=true,IgnorePublicAcls=true,BlockPublicPolicy=true,RestrictPublicBuckets=true"

aws ec2 create-key-pair --key-name klingon
```
Copy:
- Access key & secret access key generated.
- Keypair that has been generated

```
aws s3api put-bucket-policy --bucket klingon-terraform-state --policy file://../../../infrastructure/policy.json
```

```
terraform init
terraform workspace new production
terraform workspace select production
```
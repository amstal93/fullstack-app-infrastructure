resource "aws_security_group" "grafana" {
  name        = "grafana_default_security_group"
  description = "Main security group for instances in public subnet"
  vpc_id      = var.vpc_id

  ingress {
    from_port       = 0
    to_port         = 65535
    protocol        = "tcp"
    security_groups = [var.bastion_security_group]
  }

  ingress {
    from_port   = 0
    to_port     = 65535
    protocol    = "tcp"
    cidr_blocks = ["10.0.100.170/32"]
  }

  ingress {
    from_port       = 0
    to_port         = 65535
    protocol        = "tcp"
    security_groups = [var.loadbalancer_security_group]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "${var.project_name}-${terraform.workspace}-grafana-default-security-group"
  }
}

data "template_file" "init_grafana" {
  template = file("../../modules/grafana/user_data/init_grafana.tpl")

  #   vars = {
  #     elasticsearch_host = aws_instance.elasticsearch.private_ip
  #   }
}

resource "aws_instance" "grafana" {
  ami                    = var.aws_amis[var.aws_region]
  instance_type          = var.elk_instance_type
  key_name               = var.aws_key_name
  vpc_security_group_ids = [aws_security_group.grafana.id]
  subnet_id              = var.private_subnet_1

  ebs_block_device {
    device_name = "/dev/sdb"
    volume_type = "io1"
    volume_size = "10"
    iops        = "500"
  }

  user_data = data.template_file.init_grafana.rendered

  volume_tags = {
    Name = "${var.project_name}-${terraform.workspace}-grafana"
  }

  tags = {
    Name = "${var.project_name}-${terraform.workspace}-grafana"
  }

  lifecycle {
    ignore_changes = [user_data]
  }
}

resource "aws_iam_role" "grafana_dlm_lifecycle_role" {
  name = "${var.project_name}-${terraform.workspace}-grafana-dlm-lifecycle-role"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "dlm.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource "aws_iam_role_policy" "grafana_dlm_lifecycle" {
  name = "${var.project_name}-${terraform.workspace}-grafana-dlm-lifecycle-policy"
  role = "${aws_iam_role.grafana_dlm_lifecycle_role.id}"

  policy = <<EOF
{
   "Version": "2012-10-17",
   "Statement": [
      {
         "Effect": "Allow",
         "Action": [
            "ec2:CreateSnapshot",
            "ec2:DeleteSnapshot",
            "ec2:DescribeVolumes",
            "ec2:DescribeSnapshots"
         ],
         "Resource": "*"
      },
      {
         "Effect": "Allow",
         "Action": [
            "ec2:CreateTags"
         ],
         "Resource": "arn:aws:ec2:*::snapshot/*"
      }
   ]
}
EOF
}

resource "aws_dlm_lifecycle_policy" "grafana" {
  description        = "${var.project_name}-${terraform.workspace}-grafana DLM lifecycle policy"
  execution_role_arn = "${aws_iam_role.grafana_dlm_lifecycle_role.arn}"
  state              = "ENABLED"

  policy_details {
    resource_types = ["VOLUME"]

    schedule {
      name = "2 weeks of daily snapshots"

      create_rule {
        interval      = 24
        interval_unit = "HOURS"
        times         = ["23:45"]
      }

      retain_rule {
        count = 14
      }

      tags_to_add = {
        SnapshotCreator = "DLM"
      }

      copy_tags = false
    }

    target_tags = {
      Name = "${var.project_name}-${terraform.workspace}-grafana"
    }
  }
}